# TCL File Generated by Component Editor 18.1
# Wed Apr 20 16:33:33 CDT 2022
# DO NOT MODIFY


# 
# graphics "graphics" v1.31
#  2022.04.20.16:33:33
# 
# 

# 
# request TCL package from ACDS 16.1
# 
package require -exact qsys 16.1


# 
# module graphics
# 
set_module_property DESCRIPTION ""
set_module_property NAME graphics
set_module_property VERSION 1.31
set_module_property INTERNAL false
set_module_property OPAQUE_ADDRESS_MAP true
set_module_property AUTHOR ""
set_module_property DISPLAY_NAME graphics
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
set_module_property REPORT_TO_TALKBACK false
set_module_property ALLOW_GREYBOX_GENERATION false
set_module_property REPORT_HIERARCHY false


# 
# file sets
# 
add_fileset QUARTUS_SYNTH QUARTUS_SYNTH "" ""
set_fileset_property QUARTUS_SYNTH TOP_LEVEL graphics
set_fileset_property QUARTUS_SYNTH ENABLE_RELATIVE_INCLUDE_PATHS false
set_fileset_property QUARTUS_SYNTH ENABLE_FILE_OVERWRITE_MODE false
add_fileset_file graphics.sv SYSTEM_VERILOG PATH graphics.sv TOP_LEVEL_FILE

add_fileset SIM_VERILOG SIM_VERILOG "" ""
set_fileset_property SIM_VERILOG TOP_LEVEL graphics
set_fileset_property SIM_VERILOG ENABLE_RELATIVE_INCLUDE_PATHS false
set_fileset_property SIM_VERILOG ENABLE_FILE_OVERWRITE_MODE false
add_fileset_file graphics.sv SYSTEM_VERILOG PATH graphics.sv


# 
# parameters
# 


# 
# display items
# 


# 
# connection point CLK
# 
add_interface CLK clock end
set_interface_property CLK clockRate 0
set_interface_property CLK ENABLED true
set_interface_property CLK EXPORT_OF ""
set_interface_property CLK PORT_NAME_MAP ""
set_interface_property CLK CMSIS_SVD_VARIABLES ""
set_interface_property CLK SVD_ADDRESS_GROUP ""

add_interface_port CLK CLK clk Input 1


# 
# connection point RESET
# 
add_interface RESET reset end
set_interface_property RESET associatedClock CLK
set_interface_property RESET synchronousEdges DEASSERT
set_interface_property RESET ENABLED true
set_interface_property RESET EXPORT_OF ""
set_interface_property RESET PORT_NAME_MAP ""
set_interface_property RESET CMSIS_SVD_VARIABLES ""
set_interface_property RESET SVD_ADDRESS_GROUP ""

add_interface_port RESET RESET reset Input 1


# 
# connection point graphics
# 
add_interface graphics avalon end
set_interface_property graphics addressUnits WORDS
set_interface_property graphics associatedClock CLK
set_interface_property graphics associatedReset RESET
set_interface_property graphics bitsPerSymbol 8
set_interface_property graphics burstOnBurstBoundariesOnly false
set_interface_property graphics burstcountUnits WORDS
set_interface_property graphics explicitAddressSpan 0
set_interface_property graphics holdTime 0
set_interface_property graphics linewrapBursts false
set_interface_property graphics maximumPendingReadTransactions 0
set_interface_property graphics maximumPendingWriteTransactions 0
set_interface_property graphics readLatency 0
set_interface_property graphics readWaitTime 1
set_interface_property graphics setupTime 0
set_interface_property graphics timingUnits Cycles
set_interface_property graphics writeWaitTime 0
set_interface_property graphics ENABLED true
set_interface_property graphics EXPORT_OF ""
set_interface_property graphics PORT_NAME_MAP ""
set_interface_property graphics CMSIS_SVD_VARIABLES ""
set_interface_property graphics SVD_ADDRESS_GROUP ""

add_interface_port graphics AVL_ADDR address Input 10
add_interface_port graphics AVL_WRITEDATA writedata Input 32
add_interface_port graphics AVL_WRITE write Input 1
add_interface_port graphics AVL_CS chipselect Input 1
set_interface_assignment graphics embeddedsw.configuration.isFlash 0
set_interface_assignment graphics embeddedsw.configuration.isMemoryDevice 0
set_interface_assignment graphics embeddedsw.configuration.isNonVolatileStorage 0
set_interface_assignment graphics embeddedsw.configuration.isPrintableDevice 0


# 
# connection point vga_port
# 
add_interface vga_port conduit end
set_interface_property vga_port associatedClock CLK
set_interface_property vga_port associatedReset ""
set_interface_property vga_port ENABLED true
set_interface_property vga_port EXPORT_OF ""
set_interface_property vga_port PORT_NAME_MAP ""
set_interface_property vga_port CMSIS_SVD_VARIABLES ""
set_interface_property vga_port SVD_ADDRESS_GROUP ""

add_interface_port vga_port red red Output 4
add_interface_port vga_port green green Output 4
add_interface_port vga_port blue blue Output 4
add_interface_port vga_port vs vs Output 1
add_interface_port vga_port hs hs Output 1

