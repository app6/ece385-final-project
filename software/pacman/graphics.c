#include "graphics.h"

void tileTest(alt_u8 x, alt_u8 y, alt_u8 r, alt_u8 g, alt_u8 b) {
	alt_u16 index = x + y * COLS;
	graphics->tiles[index] = (r << 8) | (g << 4) | b;
}

void tileSet(alt_u8 x, alt_u8 y, alt_u8 c0, alt_u8 c1, alt_u8 c2, alt_u8 c3, alt_u8 romInd) {
	alt_u16 index = x + y * COLS;
	graphics->tiles[index] = ((c0 % 16) << 20) | ((c1 % 16) << 16) | ((c2 % 16) << 12) | ((c3 % 16) << 8) | romInd;
}
