#include "graphics.h"
#include <stdio.h>

int main() {
	/*volatile int* HEX = (int*) HEX_BASE;
	volatile short* LED = (short*) LED_BASE;
	volatile short* SW = (short*) SW_BASE;
	volatile char* KEY = (char*) KEY_BASE;

	int count = 0;
	char shift = 0;
	char last_key = 3;
	char key;

	printf("Test!\n");
	while (1) {
		*HEX = *SW << shift;
		*LED = count++ >> 14;
		key = *KEY;
		if (key != last_key) {
			if (~key & 1 && last_key & 1)
				shift = (shift + 1) % 24;
			else if (~key & 2 && last_key & 2)
				shift = (shift + 23) % 24;
			last_key = key;
		}
	}*/

	graphics->palette[0] = 0xFFF;
	graphics->palette[1] = 0x0F0;
	graphics->palette[2] = 0xF00;
	graphics->palette[3] = 0xF0F;


	int x, y, a;
	a = 0;
	while (1) {
		for (x = 0; x < COLS; x++) {
			for (y = 0; y < ROWS; y++) {
				// tileTest(x, y, (x + a) % 16, (y - (a % 16) + 16) % 16, ((x - a) & 0xC) | ((y - a) & 0x3));
				tileSet(x, y, 0, 1, 2, 3, a % 83);
			}
			a++;
		}

	}
//	tileSet(0, 0, 0, 1, 2, 3, 0x02);
	// 000000010010001100000000

	return 0;
}
