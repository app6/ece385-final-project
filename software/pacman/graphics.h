#ifndef GRAPHICS_H_
#define GRAPHICS_H_

#include <system.h>
#include <alt_types.h>
#include <stdio.h>

#define ROWS 36
#define COLS 28
#define SPRITES 16
#define PALETTE_COLORS 16

struct Graphics{
	alt_u32 tiles[ROWS * COLS];
	alt_u32 sprites[SPRITES];
	alt_u32 palette[PALETTE_COLORS];
};

static volatile struct Graphics* graphics = GRAPHICS_BASE;

void tileTest(alt_u8 x, alt_u8 y, alt_u8 r, alt_u8 g, alt_u8 b);

void tileSet(alt_u8 x, alt_u8 y, alt_u8 c0, alt_u8 c1, alt_u8 c2, alt_u8 c3, alt_u8 romInd);

#endif /* GRAPHICS_H_ */
