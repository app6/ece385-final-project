module pacman(
    ///////// Clocks /////////
    input    MAX10_CLK1_50,

    ///////// KEY /////////
    input    [ 1: 0]   KEY,

    ///////// SW /////////
    input    [ 9: 0]   SW,

    ///////// LEDR /////////
    output   [ 9: 0]   LEDR,

    ///////// HEX /////////
    output   [ 7: 0]   HEX0,
    output   [ 7: 0]   HEX1,
    output   [ 7: 0]   HEX2,
    output   [ 7: 0]   HEX3,
    output   [ 7: 0]   HEX4,
    output   [ 7: 0]   HEX5,

    ///////// SDRAM /////////
    output             DRAM_CLK,
    output             DRAM_CKE,
    output   [12: 0]   DRAM_ADDR,
    output   [ 1: 0]   DRAM_BA,
    inout    [15: 0]   DRAM_DQ,
    output             DRAM_LDQM,
    output             DRAM_UDQM,
    output             DRAM_CS_N,
    output             DRAM_WE_N,
    output             DRAM_CAS_N,
    output             DRAM_RAS_N,

    ///////// VGA /////////
    output             VGA_HS,
    output             VGA_VS,
    output   [ 3: 0]   VGA_R,
    output   [ 3: 0]   VGA_G,
    output   [ 3: 0]   VGA_B,

    ///////// ARDUINO /////////
    inout    [15: 0]   ARDUINO_IO,
    inout              ARDUINO_RESET_N
);

logic [23:0] HEX;

assign HEX0[7] = 1'b1;
assign HEX1[7] = 1'b1;
assign HEX2[7] = 1'b1;
assign HEX3[7] = 1'b1;
assign HEX4[7] = 1'b1;
assign HEX5[7] = 1'b1;

HexDriver hex_driver0(.In0(HEX[ 3: 0]), .Out0(HEX0[6:0]));
HexDriver hex_driver1(.In0(HEX[ 7: 4]), .Out0(HEX1[6:0]));
HexDriver hex_driver2(.In0(HEX[11: 8]), .Out0(HEX2[6:0]));
HexDriver hex_driver3(.In0(HEX[15:12]), .Out0(HEX3[6:0]));
HexDriver hex_driver4(.In0(HEX[19:16]), .Out0(HEX4[6:0]));
HexDriver hex_driver5(.In0(HEX[23:20]), .Out0(HEX5[6:0]));

// command
logic  command_valid;
logic  [4:0] command_channel;
logic  command_startofpacket;
logic  command_endofpacket;
logic command_ready;
logic sample_clk;
logic [11:0] x_val;
logic [11:0] y_val;

// continused send command
assign command_startofpacket = 1'b1; // // ignore in altera_adc_control core
assign command_endofpacket = 1'b1; // ignore in altera_adc_control core
assign command_valid = 1'b1; //
//assign command_channel = 5'h01; 

////////////////////////////////////////////
// response
wire response_valid/* synthesis keep */;
wire [4:0] response_channel;
wire [11:0] response_data;
wire response_startofpacket;
wire response_endofpacket;
reg [4:0]  cur_adc_ch /* synthesis noprune */;
reg [11:0] adc_sample_data /* synthesis noprune */;
reg [12:0] vol /* synthesis noprune */;


soc nois2_soc(
    .clk_clk(MAX10_CLK1_50),
    .key_export(KEY),
   //.keycode_export,
    .reset_reset_n(1'b1),
    .sdram_clk_clk(DRAM_CLK),
    .sdram_wire_addr(DRAM_ADDR),
    .sdram_wire_ba(DRAM_BA),
    .sdram_wire_cas_n(DRAM_CAS_N),
   .sdram_wire_cke(DRAM_CKE),
   .sdram_wire_cs_n(DRAM_CS_N),
   .sdram_wire_dq(DRAM_DQ),
   .sdram_wire_dqm({DRAM_UDQM, DRAM_LDQM}),
   .sdram_wire_ras_n(DRAM_RAS_N),
   .sdram_wire_we_n(DRAM_WE_N),
   //.spi0_MISO,
   //.spi0_MOSI,
   //.spi0_SCLK,
   //.spi0_SS_n,
   //.usb_gpx_export,
   //.usb_irq_export,
   //.usb_rst_export
    .hex_export(HEX),
    .led_export(LEDR),
    .sw_export(SW),
    .vga_port_red(VGA_R),
	.vga_port_green(VGA_G),
	.vga_port_blue(VGA_B),
	.vga_port_vs(VGA_VS),
	.vga_port_hs(VGA_HS),
    .modular_adc_0_command_valid          (command_valid),          //  modular_adc_0_command.valid
    .modular_adc_0_command_channel        (command_channel),        //                       .channel
    .modular_adc_0_command_startofpacket  (command_startofpacket),  //                       .startofpacket
    .modular_adc_0_command_endofpacket    (command_endofpacket),    //                       .endofpacket
    .modular_adc_0_command_ready          (command_ready),          //                       .ready
    .modular_adc_0_response_valid         (response_valid),         // modular_adc_0_response.valid
    .modular_adc_0_response_channel       (response_channel),       //                       .channel
    .modular_adc_0_response_data          (response_data),          //                       .data
    .modular_adc_0_response_startofpacket (response_startofpacket), //                       .startofpacket
    .modular_adc_0_response_endofpacket   (response_endofpacket),    //                       .endofpacket
	 .joystick_external_connection_export  ({4'h0, x_val, 4'h0, y_val})
);

//assign command_channel = counter[15] + 1;

always @ (posedge MAX10_CLK1_50)
begin
	//counter <= counter + 1;
	if (response_valid)
	begin
		if (response_channel == 5'h01)
			command_channel <= 5'h02;
		else
			command_channel <= 5'h01;
      if (response_channel == 5'h01)
		begin
		   y_val <= response_data;
		end
		else
		begin
			x_val <= response_data;
		end
	end
end

/*assign LEDR[0] = response_valid;
assign LEDR[5:1] = response_channel;

// adc_sample_data: hold 12-bit adc sample value
// Vout = Vin (12-bit x2 x 2500 / 4095)	

assign HEX3[7] = 1'b1; // low active
assign HEX2[7] = 1'b1; // low active
assign HEX1[7] = 1'b1; // low active
assign HEX0[7] = 1'b1; // low active

HexDriver hex5 (.In0(x_val[11:8]), .Out0(HEX5));
HexDriver hex4 (.In0(x_val[7:4]), .Out0(HEX4));
HexDriver hex3 (.In0(x_val[3:0]), .Out0(HEX3));
HexDriver hex2 (.In0(y_val[11:8]), .Out0(HEX2));
HexDriver hex1 (.In0(y_val[7:4]), .Out0(HEX1));
HexDriver hex0 (.In0(y_val[3:0]), .Out0(HEX0));

// assign HEX5 = 8'b11111111;
// assign HEX4 = 8'b11111111;

// HexDriver	SEG7_LUT_v (
// 	.Out0(HEX3),
// 	.In0(vol/1000)
// );

// HexDriver	SEG7_LUT_v_1 (
// 	.Out0(HEX2),
// 	.In0(vol/100 - (vol/1000)*10)
// );

// HexDriver	SEG7_LUT_v_2 (
// 	.Out0(HEX1),
// 	.In0(vol/10 - (vol/100)*10)
// );

// HexDriver	SEG7_LUT_v_3 (
// 	.Out0(HEX0),
// 	.In0(vol - (vol/10)*10)
// );*/


endmodule