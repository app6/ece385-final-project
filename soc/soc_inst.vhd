	component soc is
		port (
			clk_clk                              : in    std_logic                     := 'X';             -- clk
			hex_export                           : out   std_logic_vector(23 downto 0);                    -- export
			joystick_external_connection_export  : in    std_logic_vector(31 downto 0) := (others => 'X'); -- export
			key_export                           : in    std_logic_vector(1 downto 0)  := (others => 'X'); -- export
			keycode_export                       : out   std_logic_vector(7 downto 0);                     -- export
			led_export                           : out   std_logic_vector(9 downto 0);                     -- export
			modular_adc_0_command_valid          : in    std_logic                     := 'X';             -- valid
			modular_adc_0_command_channel        : in    std_logic_vector(4 downto 0)  := (others => 'X'); -- channel
			modular_adc_0_command_startofpacket  : in    std_logic                     := 'X';             -- startofpacket
			modular_adc_0_command_endofpacket    : in    std_logic                     := 'X';             -- endofpacket
			modular_adc_0_command_ready          : out   std_logic;                                        -- ready
			modular_adc_0_response_valid         : out   std_logic;                                        -- valid
			modular_adc_0_response_channel       : out   std_logic_vector(4 downto 0);                     -- channel
			modular_adc_0_response_data          : out   std_logic_vector(11 downto 0);                    -- data
			modular_adc_0_response_startofpacket : out   std_logic;                                        -- startofpacket
			modular_adc_0_response_endofpacket   : out   std_logic;                                        -- endofpacket
			reset_reset_n                        : in    std_logic                     := 'X';             -- reset_n
			sample_clk_clk                       : out   std_logic;                                        -- clk
			sdram_clk_clk                        : out   std_logic;                                        -- clk
			sdram_wire_addr                      : out   std_logic_vector(12 downto 0);                    -- addr
			sdram_wire_ba                        : out   std_logic_vector(1 downto 0);                     -- ba
			sdram_wire_cas_n                     : out   std_logic;                                        -- cas_n
			sdram_wire_cke                       : out   std_logic;                                        -- cke
			sdram_wire_cs_n                      : out   std_logic;                                        -- cs_n
			sdram_wire_dq                        : inout std_logic_vector(15 downto 0) := (others => 'X'); -- dq
			sdram_wire_dqm                       : out   std_logic_vector(1 downto 0);                     -- dqm
			sdram_wire_ras_n                     : out   std_logic;                                        -- ras_n
			sdram_wire_we_n                      : out   std_logic;                                        -- we_n
			spi0_MISO                            : in    std_logic                     := 'X';             -- MISO
			spi0_MOSI                            : out   std_logic;                                        -- MOSI
			spi0_SCLK                            : out   std_logic;                                        -- SCLK
			spi0_SS_n                            : out   std_logic;                                        -- SS_n
			sw_export                            : in    std_logic_vector(9 downto 0)  := (others => 'X'); -- export
			usb_gpx_export                       : in    std_logic                     := 'X';             -- export
			usb_irq_export                       : in    std_logic                     := 'X';             -- export
			usb_rst_export                       : out   std_logic;                                        -- export
			vga_port_red                         : out   std_logic_vector(3 downto 0);                     -- red
			vga_port_green                       : out   std_logic_vector(3 downto 0);                     -- green
			vga_port_blue                        : out   std_logic_vector(3 downto 0);                     -- blue
			vga_port_vs                          : out   std_logic;                                        -- vs
			vga_port_hs                          : out   std_logic                                         -- hs
		);
	end component soc;

	u0 : component soc
		port map (
			clk_clk                              => CONNECTED_TO_clk_clk,                              --                          clk.clk
			hex_export                           => CONNECTED_TO_hex_export,                           --                          hex.export
			joystick_external_connection_export  => CONNECTED_TO_joystick_external_connection_export,  -- joystick_external_connection.export
			key_export                           => CONNECTED_TO_key_export,                           --                          key.export
			keycode_export                       => CONNECTED_TO_keycode_export,                       --                      keycode.export
			led_export                           => CONNECTED_TO_led_export,                           --                          led.export
			modular_adc_0_command_valid          => CONNECTED_TO_modular_adc_0_command_valid,          --        modular_adc_0_command.valid
			modular_adc_0_command_channel        => CONNECTED_TO_modular_adc_0_command_channel,        --                             .channel
			modular_adc_0_command_startofpacket  => CONNECTED_TO_modular_adc_0_command_startofpacket,  --                             .startofpacket
			modular_adc_0_command_endofpacket    => CONNECTED_TO_modular_adc_0_command_endofpacket,    --                             .endofpacket
			modular_adc_0_command_ready          => CONNECTED_TO_modular_adc_0_command_ready,          --                             .ready
			modular_adc_0_response_valid         => CONNECTED_TO_modular_adc_0_response_valid,         --       modular_adc_0_response.valid
			modular_adc_0_response_channel       => CONNECTED_TO_modular_adc_0_response_channel,       --                             .channel
			modular_adc_0_response_data          => CONNECTED_TO_modular_adc_0_response_data,          --                             .data
			modular_adc_0_response_startofpacket => CONNECTED_TO_modular_adc_0_response_startofpacket, --                             .startofpacket
			modular_adc_0_response_endofpacket   => CONNECTED_TO_modular_adc_0_response_endofpacket,   --                             .endofpacket
			reset_reset_n                        => CONNECTED_TO_reset_reset_n,                        --                        reset.reset_n
			sample_clk_clk                       => CONNECTED_TO_sample_clk_clk,                       --                   sample_clk.clk
			sdram_clk_clk                        => CONNECTED_TO_sdram_clk_clk,                        --                    sdram_clk.clk
			sdram_wire_addr                      => CONNECTED_TO_sdram_wire_addr,                      --                   sdram_wire.addr
			sdram_wire_ba                        => CONNECTED_TO_sdram_wire_ba,                        --                             .ba
			sdram_wire_cas_n                     => CONNECTED_TO_sdram_wire_cas_n,                     --                             .cas_n
			sdram_wire_cke                       => CONNECTED_TO_sdram_wire_cke,                       --                             .cke
			sdram_wire_cs_n                      => CONNECTED_TO_sdram_wire_cs_n,                      --                             .cs_n
			sdram_wire_dq                        => CONNECTED_TO_sdram_wire_dq,                        --                             .dq
			sdram_wire_dqm                       => CONNECTED_TO_sdram_wire_dqm,                       --                             .dqm
			sdram_wire_ras_n                     => CONNECTED_TO_sdram_wire_ras_n,                     --                             .ras_n
			sdram_wire_we_n                      => CONNECTED_TO_sdram_wire_we_n,                      --                             .we_n
			spi0_MISO                            => CONNECTED_TO_spi0_MISO,                            --                         spi0.MISO
			spi0_MOSI                            => CONNECTED_TO_spi0_MOSI,                            --                             .MOSI
			spi0_SCLK                            => CONNECTED_TO_spi0_SCLK,                            --                             .SCLK
			spi0_SS_n                            => CONNECTED_TO_spi0_SS_n,                            --                             .SS_n
			sw_export                            => CONNECTED_TO_sw_export,                            --                           sw.export
			usb_gpx_export                       => CONNECTED_TO_usb_gpx_export,                       --                      usb_gpx.export
			usb_irq_export                       => CONNECTED_TO_usb_irq_export,                       --                      usb_irq.export
			usb_rst_export                       => CONNECTED_TO_usb_rst_export,                       --                      usb_rst.export
			vga_port_red                         => CONNECTED_TO_vga_port_red,                         --                     vga_port.red
			vga_port_green                       => CONNECTED_TO_vga_port_green,                       --                             .green
			vga_port_blue                        => CONNECTED_TO_vga_port_blue,                        --                             .blue
			vga_port_vs                          => CONNECTED_TO_vga_port_vs,                          --                             .vs
			vga_port_hs                          => CONNECTED_TO_vga_port_hs                           --                             .hs
		);

