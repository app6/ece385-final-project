#include "game.h"
#include "pacman.h"
#include "ghost.h"
#include "graphics.h"
#include <system.h>
#include <stdlib.h>

#define DOT_POINTS 10
#define POWER_POINTS 50
#define GHOST_POINTS 200
#define NUM_DOTS 244
#define SCORE_DIGITS 6
#define READY_TIME 60
#define LVL_PAUSE_TIME 60
#define FLASH_LENGTH 6
#define FLASH_MAX_LVL 17
#define EXTRA_LIFE_POINTS 10000
#define DEATH_TIME 120

static alt_u8 level;
static alt_u8 has_dot[COLS][ROWS];
static alt_u8 has_power[COLS][ROWS];
static alt_u32 score;
static alt_u8 dots_left;
static alt_u32 frames;
static alt_u32 flash_frames;
static alt_u8 flashes;
static alt_u8 lives;
static alt_u32 death_time;

static const alt_u8 FLASHES_LVL[] = {
	5, 5, 5, 5,
	5, 5, 5, 5,
	3, 5, 5, 3,
	3, 5, 3, 3,
	0, 3
};

alt_u8 get_lvl() {
	return level;
}

alt_u8 is_wall(alt_u8 x, alt_u8 y) {
	return (WALL_DATA[y] >> (COLS - 1 - x)) & 1;
}

alt_u8 is_ghost_wall(alt_u8 x, alt_u8 y) {
	return (GHOST_WALL_DATA[y] >> (COLS - 1 - x)) & 1;
}

alt_u8 is_dot(alt_u8 x, alt_u8 y) {
	return has_dot[x][y];
}

alt_u8 is_power(alt_u8 x, alt_u8 y) {
	return has_power[x][y];
}

alt_u8 can_turn(alt_u8 x, alt_u8 y) {
	return (~NO_TURN_DATA[y] >> (COLS - 1 - x)) & 1;
}

alt_u32 get_frames() {
	return frames - READY_TIME;
}

alt_u8 get_dots_left() {
	return dots_left;
}

alt_u8 is_opposite_dir(enum input a, enum input b) {
	return (a == UP && b == DOWN) || (a == DOWN && b == UP) || (a == RIGHT && b == LEFT) || (a == LEFT && b == RIGHT);
}

enum input opposite_dir(enum input inp) {
	switch (inp){
		case UP: return DOWN;
		case DOWN: return UP;
		case LEFT: return RIGHT;
		case RIGHT: return LEFT;
	}
}

void add_points(alt_u32 points) {
	if (score < EXTRA_LIFE_POINTS && score + points >= EXTRA_LIFE_POINTS) {
		set_lives(++lives);
	}

	score += points;

	alt_u8 colors[4] = {BLACK, WHITE, BLACK, BLACK};
	alt_u32 remainder = score;
	alt_u8 i;
	for (i = 0; i < SCORE_DIGITS; i++) {
		draw_tile(0x2D + remainder % 10, 6 - i, 1, colors);
		draw_tile(0x2D + remainder % 10, 16 - i, 1, colors);
		remainder /= 10;
		if (remainder == 0)
			break;
	}

}

alt_u8 eat(alt_u8 x, alt_u8 y) {
	if (is_dot(x, y)) {
		has_dot[x][y] = 0;
		add_points(DOT_POINTS);
		dots_left--;
		return 1;
	}
	else if (is_power(x, y)) {
		power_pellete();
		has_power[x][y] = 0;
		add_points(POWER_POINTS);
		dots_left--;
		return 1;
	}

	return 0;
}

void eat_ghost(alt_u8 num) {
	add_points(GHOST_POINTS << num);
}

void die() {
	if (!(--lives)) {
		initialize();
		return;
	}
	
	death_time = DEATH_TIME;
}

void next_level() {
	level++;
	flashes = (level > FLASH_MAX_LVL ? 0 : FLASHES_LVL[level]);
	frames = 0;
	flash_frames = -1;
	dots_left = NUM_DOTS;

	int x, y;
	for (x = 0; x < COLS; x++) {
		for (y = 0; y < ROWS; y++) {
			has_dot[x][y] = (DOT_DATA[y] >> (COLS - 1 - x)) & 1;
			has_power[x][y] = (POWER_DATA[y] >> (COLS - 1 - x)) & 1;
		}
	}

	initialize_pacman();
	initialize_ghosts();
	
	initialize_background();
	initialize_palette();
	add_points(0);
	set_lives(lives);
}

void initialize() {
	level = -1;
	score = 0;
	lives = 5;
	next_level();
}

void update() {
	if (death_time) {
		if (!(--death_time)) {
			initialize_background();
			initialize_pacman();
			initialize_ghosts();
			add_points(0);

			set_lives(lives);
			frames = 0;
		}
		return;
	}
	if (!dots_left) {
		if (flash_frames == (alt_u32) -1)
			flash_frames = LVL_PAUSE_TIME + FLASH_LENGTH * flashes * 2;
		if (flash_frames > 0) {
			flash_frames--;
			if (flash_frames < FLASH_LENGTH * flashes * 2) {
				alt_u8 flip = (flash_frames / flashes) % 2;
				alt_u8 index = (flip ? WHITE : BLUE);
				set_palette(BLUE, PALETTE_COLORS[index]);
			}
		}
		if (flash_frames == 0)
			next_level();	
		return;
	}
	if (frames < READY_TIME) {
		frames++;
		return;
	}
	else if (frames == READY_TIME)
		erase_ready();

	srand(frames << 8 + level);
	update_pacman();
	update_ghosts(curr_inp, pacman_x, pacman_y);

	frames++;
}
