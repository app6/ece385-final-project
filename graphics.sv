module graphics(
	input		logic				CLK,
	input		logic				RESET,
	input		logic [10:0]	AVL_ADDR,
	input		logic [31:0]	AVL_WRITEDATA,
	input		logic				AVL_WRITE,
	input		logic				AVL_CS,
	output	logic [3:0]		red, green, blue,
	output	logic				hs, vs
);

logic [9:0] VRAM_ADDR;
logic [31:0] VRAM_DATA;
logic VGA_CLK, blank;
logic [9:0] X, Y;
logic [9:0] GAME_X, GAME_Y;
logic [3:0] c0, c1, c2, c3;
logic [7:0] romInd;
logic [15:0] pixelRow;
logic [31:0] palette [16];

assign GAME_X = X - 10'h0D0;
assign GAME_Y = Y - 10'h060;

vram vram0(
	.clock(CLK),
	.data(AVL_WRITEDATA),
	.rdaddress(VRAM_ADDR),
	.wraddress(AVL_ADDR),
	.wren(AVL_CS & AVL_WRITE),
	.q(VRAM_DATA)
);

vga_controller vga(
	.Clk(CLK),
	.Reset(RESET),
	.hs(hs),
	.vs(vs),
	.pixel_clk(VGA_CLK),
	.blank(blank),
	//.sync(),
	.DrawX(X),
	.DrawY(Y)
);

backgroundROM bgROM(
	.addr({romInd[6:0], GAME_Y[2:0]}),
	.data_out(pixelRow)
);

assign VRAM_ADDR = GAME_Y[9:3] * 28 + GAME_X[9:3];
assign {c0, c1, c2, c3, romInd} = VRAM_DATA[23:0];

always_ff @(posedge CLK)
begin 
	if (AVL_ADDR[10] & AVL_WRITE & AVL_CS)
	begin
		palette[AVL_ADDR[3:0]] <= AVL_WRITEDATA;
	end
end

always_comb
begin
	if (blank == 1'b1 && GAME_X < 10'h0E0 && GAME_Y < 10'h120)
	begin
		//Draw Game
		unique case ({pixelRow[15 - (GAME_X[2:0]*2)], pixelRow[15 - (GAME_X[2:0]*2 + 1)]})
			2'b00: begin
				{red, green, blue} = palette[c0][11:0];
			end
			2'b01: begin
				{red, green, blue} = palette[c1][11:0];
			end
			2'b10: begin
				{red, green, blue} = palette[c2][11:0];
			end
			2'b11: begin
				{red, green, blue} = palette[c3][11:0];
			end
		endcase
	end
	else
	begin
		red = 4'h0;
		green = 4'h0;
		blue = 4'h0;
	end
end

endmodule